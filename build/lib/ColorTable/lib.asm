
    ;

;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;//
ColorTable.BuildColorTable:

            clx
            stz ColorTable.lo
            stz ColorTable.hi

.loop.lowerHalf

            lda ColorTable.lo,x
            clc
            adc #$01
            sta ColorTable.lo+1,x
            lda ColorTable.hi,x
            adc #$00
            sta ColorTable.hi+1,x

            inx
        bne .loop.lowerHalf


            lda ColorTable.lo+255
            clc
            adc #$01
            sta ColorTable.lo+256
            lda ColorTable.hi+255
            adc #$00
            sta ColorTable.hi+256

.loop.upperHalf

            lda ColorTable.lo+256,x
            clc
            adc #$01
            sta ColorTable.lo+256+1,x
            lda ColorTable.hi+256,x
            adc #$00
            sta ColorTable.hi+256+1,x

            inx
        bne .loop.upperHalf

.out
    rts


;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// Map is 64x32
ColorTable.BuildColorBAT:

            VDC.reg  MAWR, #$0000
            VDC.reg  VWR
            MOVE.b  #VWR, <vdc_reg

            ldy #$20
.lineLoop
            MOVE.b #$03, <R0
            MOVE.b #$0A, <R1
            MOVE.w #$400, AX

.innerLoop
            stx $0002
            sta $0003
            dec <R1
        bne .innerLoop
        pha
            MOVE.b #$0A, <R1
        pla
            inx
            dec <R0
        bne .innerLoop

            ldx #$22
.fillCopy
            st1 #$03
            st2 #$04
            dex
        bne .fillCopy

            dey
        bne .lineLoop

    rts


;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;//
ColorTable.BuildColorTiles:

            VDC.reg  MAWR, #$4000
            VDC.reg  VWR
            MOVE.b  #VWR, <vdc_reg

; First tile is color #1
            ldx #$08
.firstColorComp0
            st1 #$ff
            st2 #$00
            dex
        bne .firstColorComp0
            ldx #$08
.firstColorComp1
            st1 #$00
            st2 #$00
            dex
        bne .firstColorComp1


; First tile is color #2
            ldx #$08
.secondColorComp0
            st1 #$00
            st2 #$ff
            dex
        bne .secondColorComp0
            ldx #$08
.secondColorComp1
            st1 #$00
            st2 #$00
            dex
        bne .secondColorComp1

; First tile is color #3
            ldx #$08
.thirdColorComp0
            st1 #$ff
            st2 #$ff
            dex
        bne .thirdColorComp0
            ldx #$08
.thirdColorComp1
            st1 #$00
            st2 #$00
            dex
        bne .thirdColorComp1

    rts


