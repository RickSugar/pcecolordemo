


;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// 
HsyncISR.IRQ:


;............................................
.hsync
    pha
            lda #$01
            sta $402
            stz $403
            lda <ColorTable.RGB.lo
            sta $404
            lda <ColorTable.RGB.hi
            sta $405
            lda <ColorTable.RGB.lo+1
            sta $404
            lda <ColorTable.RGB.hi+1
            sta $405
            lda <ColorTable.RGB.lo+2
            sta $404
            lda <ColorTable.RGB.hi+2
            sta $405

    phx
    phy
            lda IRQ.ackVDC
            sta <vdc_status
            bit #$20
        bne .vsync
    

            st0 #RCR
            lda <RCRline
            inc a
            sta <RCRline
            cmp #$ff
        beq .out
.updateLine
            sta $0002
            st2 #$00

        jsr HsyncISR.getNextValues

.out
            lda <vdc_reg
            sta $0000
    ply
    plx
    pla

    rti

;............................................
.vsync
            lda #$40
            sta <RCRline
            st0 #RCR
            sta $0002
            st2 #$00
            jsr HsyncISR.getNextValues
        jmp .out


;............................................
HsyncISR.getNextValues

            lda <RCRline
            cmp #$47
        beq .startingMarker
            cmp #$49
        bcc .clearSet
            cmp #$f4
        bcc .nextSet
            cmp #$f5
        beq .endingMarker
        bra .clearSet

.startingMarker
            MOVE.b #$49, ColorTable.RGB.lo
            MOVE.b #$00, ColorTable.RGB.hi
            MOVE.b #$24, ColorTable.RGB.lo+1
            MOVE.b #$01, ColorTable.RGB.hi+1
            MOVE.b #$ff, ColorTable.RGB.lo+2
            MOVE.b #$01, ColorTable.RGB.hi+2
    rts
.endingMarker
            MOVE.b #$ff, ColorTable.RGB.lo
            MOVE.b #$01, ColorTable.RGB.hi
            MOVE.b #$24, ColorTable.RGB.lo+1
            MOVE.b #$01, ColorTable.RGB.hi+1
            MOVE.b #$49, ColorTable.RGB.lo+2
            MOVE.b #$00, ColorTable.RGB.hi+2
    rts
.clearSet
            MOVE.w #ColorTable.lo, <ColorTable.Ptr.lo
            MOVE.w #ColorTable.hi, <ColorTable.Ptr.hi
            MOVE.b #$00, ColorTable.RGB.lo
            MOVE.b #$00, ColorTable.RGB.hi
            MOVE.b #$00, ColorTable.RGB.lo+1
            MOVE.b #$00, ColorTable.RGB.hi+1
            MOVE.b #$00, ColorTable.RGB.lo+2
            MOVE.b #$00, ColorTable.RGB.hi+2
    rts

.nextSet
            clx
            cly
            lda [ColorTable.Ptr.lo],y
            sta <ColorTable.RGB.lo,x
            lda [ColorTable.Ptr.hi],y
            sta <ColorTable.RGB.hi,x
            inx
            iny

            lda [ColorTable.Ptr.lo],y
            sta <ColorTable.RGB.lo,x
            lda [ColorTable.Ptr.hi],y
            sta <ColorTable.RGB.hi,x
            inx
            iny

            lda [ColorTable.Ptr.lo],y
            sta <ColorTable.RGB.lo,x
            lda [ColorTable.Ptr.hi],y
            sta <ColorTable.RGB.hi,x
            inx
            iny

            tya
            clc
            adc <ColorTable.Ptr.lo
            sta <ColorTable.Ptr.lo
            lda <ColorTable.Ptr.lo+1
            adc #$00
            sta <ColorTable.Ptr.lo+1

            txa
            clc
            adc <ColorTable.Ptr.hi
            sta <ColorTable.Ptr.hi
            lda <ColorTable.Ptr.hi+1
            adc #$00
            sta <ColorTable.Ptr.hi+1
    rts

