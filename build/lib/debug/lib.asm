



;................................................................................................
;................................................................................................
;................................................................................................
;................................................................................................
;



;......................................................................
show_debug:
;//For now, just update the SATB with the sprite pos/attr

            sVDC_INC INC_1
            sVDC_REG MAWR , $7FE0
            sVDC_REG VWR
            iVDC_PORT $0048                        ;x
            iVDC_PORT $0040                        ;y
            iVDC_PORT $03e0                        ;pattern
            iVDC_PORT SIZE16_16 | PRIOR_H |    SPAL16                    ;attrib
            iVDC_PORT $0048                        ;x
            iVDC_PORT $0050                        ;y
            iVDC_PORT $03e2                        ;pattern
            iVDC_PORT SIZE16_16 | PRIOR_H |    SPAL16                    ;attrib
            iVDC_PORT $0048                        ;x
            iVDC_PORT $0060                        ;y
            iVDC_PORT $03e4                        ;pattern
            iVDC_PORT SIZE16_16 | PRIOR_H |    SPAL16                    ;attrib
            iVDC_PORT $0048                        ;x
            iVDC_PORT $0070                        ;y
            iVDC_PORT $03e6                        ;pattern
            iVDC_PORT SIZE16_16 | PRIOR_H |    SPAL16                    ;attrib
            iVDC_PORT $0048                        ;x
            iVDC_PORT $0080                        ;y
            iVDC_PORT $03e8                        ;pattern
            iVDC_PORT SIZE16_16 | PRIOR_H |    SPAL16                    ;attrib
            iVDC_PORT $0048                        ;x
            iVDC_PORT $0090                        ;y
            iVDC_PORT $03ea                        ;pattern
            iVDC_PORT SIZE16_16 | PRIOR_H |    SPAL16                    ;attrib
            iVDC_PORT $0048                        ;x
            iVDC_PORT $00a0                        ;y
            iVDC_PORT $03ec                        ;pattern
            iVDC_PORT SIZE16_16 | PRIOR_H |    SPAL16                    ;attrib
            iVDC_PORT $0048                        ;x
            iVDC_PORT $00b0                        ;y
            iVDC_PORT $03ee                        ;pattern
            iVDC_PORT SIZE16_16 | PRIOR_H |    SPAL16                    ;attrib

            ldy #$10
            lda #$f0
            sta $402
            lda #$01
            sta $403
            lda #$33
.loop
            sta $404
            sta $405
            dey
            bne .loop


        rts
;#end


;......................................................................
UpdateDebugOSD:

    .bss
            debugcounter:    .ds 1
    .code

            sVDC_INC INC_1
            sVDC_REG MAWR , $7c00
            sVDC_REG VWR


            lda PlayerBox.x1
            jsr .copy
            lda PlayerBox.y1
            jsr .copy

            sVDC_REG MAWR , $7c40
            sVDC_REG VWR
            lda CollisionState
            jsr .copy

            sVDC_REG MAWR , $7c80
            sVDC_REG VWR
            lda [player_x.l]
            jsr .copy
            lda [player_y.l]
            jsr .copy

        rts


.copy
            pha
            and #$0f
            asl a
            asl a
            asl a
            tax
            pla
            and #$f0
            lsr a
            tay
            lda #$08
            sta debugcounter
.loop
            lda debug_font,x
            sta $0002
            lda debug_font,y
            sta $0003
            iny
            inx
            dec debugcounter
            bne .loop
        rts

