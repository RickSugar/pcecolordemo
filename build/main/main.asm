;
;
;    {Assemble with PCEAS: ver 3.23 or higher}
;
;   Turboxray '20
;



;..............................................................................................................
;..............................................................................................................
;..............................................................................................................
;..............................................................................................................

  list
  mlist

;..............................................
;                                              .
;  Logical Memory Map:                         .
;                                              .
;            $0000 = Hardware bank             .
;            $2000 = Sys Ram                   .
;            $4000 = Subcode                   .
;            $6000 = Cont. of Subcode          .
;            $8000 = Data                      .
;            $A000 = Cont. of Data             .
;            $C000 = Main                      .
;            $E000 = Fixed Libray              .
;                                              .
;..............................................


;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;//  Equates and Vars


    ;// Varibles defines
    .include "../base_func/vars.inc"
    .include "../base_func/video/vdc/vars.inc"
    .include "../base_func/video/vdc/sprites/vars.inc"
    .include "../base_func/IO/irq_controller/vars.inc"
    .include "../base_func/audio/wsg/vars.inc"
    .include "../base_func/IO/gamepad/vars.inc"


    .include "../lib/controls/vars.inc"
    .include "../lib/ColorTable/vars.inc"
    .include "../lib/HsyncISR/vars.inc"

;....................................
        .code

        .bank $00, "Fixed Lib/Start up"
        .org $e000
;....................................

;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// Support files: equates and macros


    ;// Support files for MAIN
    .include "../base_func/base.inc"
    .include "../base_func/video/video.inc"
    .include "../base_func/video/vdc/vdc.inc"
    .include "../base_func/video/vce/vce.inc"
    .include "../base_func/timer/timer.inc"
    .include "../base_func/IO/irq_controller/irq.inc"
    .include "../base_func/IO/mapper/mapper.inc"
    .include "../base_func/audio/wsg/wsg.inc"
    .include "../base_func/IO/gamepad/gamepad.inc"

    .include "../lib/controls/controls.inc"
    .include "../lib/ColorTable/colortable.inc"
    .include "../lib/HsyncISR/hsync.inc"

;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// Startup and fix lib @$E000

startup:
        ;................................
        ;Main initialization routine.
        InitialStartup
        CallFarWide init_audio
        CallFarWide init_video

        stz $2000
        tii $2000,$2001,$2000

        ;................................
        ;Set video parameters
        VCE.reg LOW_RES|H_FILTER_ON        
        VDC.reg HSR  , #$0202
        VDC.reg HDR  , #$031F
        VDC.reg VSR  , #$0F02
        VDC.reg VDR  , #$00EF
        VDC.reg VDE  , #$0003
        VDC.reg DCR  , #AUTO_SATB_ON
        VDC.reg CR   , #$0000
        VDC.reg SATB , #$7F00
        VDC.reg MWR  , #SCR64_32

        IRQ.control IRQ2_ON|VIRQ_ON|TIRQ_OFF

        TIMER.port  _7.00khz
        TIMER.cmd   TMR_OFF

        MAP_BANK #MAIN, MPR6
        jmp MAIN

;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// Data / fixed bank


;Stuff for printing on screen
  .include "../base_func/video/print/lib.asm"

;other basic functions
  .include "../base_func/video/vdc/lib.asm"

; Lib stuffs
  .include "../lib/controls/lib.asm"
  .include "../lib/HsyncISR/lib.asm"
  .include "../base_func/IO/gamepad/lib.asm"

;end DATA
;//...................................................................


;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// Interrupt routines

;//........
TIRQ.custom
    jmp [timer_vect]

TIRQ:   ;// Not used
        BBS2 <vector_mask, TIRQ.custom
        stz $1403
        rti

;//........
BRK.custom
    jmp [brk_vect]
BRK:
        BBS1 <vector_mask, BRK.custom
        rti

;//........
VDC.custom
    jmp [vdc_vect]

VDC:
        BBS0 <vector_mask, VDC.custom
          pha
        lda IRQ.ackVDC
        sta <vdc_status
        bit #$20
        bne VDC.vsync
VDC.hsync
        BBS3 <vector_mask, VDC.custom.hsync
        BBS5 <vdc_status, VDC.vsync
          pla
        rti

VDC.custom.hsync
    jmp [vdc_hsync]

VDC.custom.vsync
    jmp [vdc_vsync]

VDC.vsync
        phx
        phy
        BBS4 <vector_mask, VDC.custom.vsync

VDC.vsync.rtn
        ply
        plx
        pla
      stz __vblank
  rti

;//........
NMI:
        rti

;end INT

;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// INT VECTORS

  .org $fff6

  .dw BRK
  .dw VDC
  .dw TIRQ
  .dw NMI
  .dw startup

;..............................................................................................................
;..............................................................................................................
;..............................................................................................................
;..............................................................................................................
;Bank 0 end





;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// Main code bank @ $C000

  .bank $01, "MAIN"
  .org $c000

MAIN:
        ;................................
        ;Turn display on
        VDC.reg CR , #(BG_ON|SPR_OFF|VINT_ON|HINT_ON)

        ;................................
        ;Load font
        loadCellToVram Font, $1000
        loadCellToCram.BG Font, 0

        ;................................
        ;Clear map
        jsr ClearScreen.64x32

        ;...............................
        ; TIRQ is already enabled, but TIMER needs to be as well
        TIMER.port  _7.00khz
        TIMER.cmd   TMR_ON

        ;...............................
        ; Set the DDA routine and music engine ISRs
        ISR.setVector VDC_VEC , HsyncISR.IRQ
        ISR.setVecMask VDC_VEC

        ;...............................
        ; Start with the first line of the display
        VDC.reg RCR, #$40

        ;...............................
        ; Set screen data
        CallFar ColorTable.BuildColorTable
        CallFar ColorTable.BuildColorTiles
        CallFar ColorTable.BuildColorBAT
        MOVE.w #ColorTable.lo, <ColorTable.Ptr.lo
        MOVE.w #ColorTable.hi, <ColorTable.Ptr.hi

        ;................................
        ;start the party
        Interrupts.enable

        ;PRINT_STR_i "Init build",2,3

main_loop:

        WAITVBLANK 0
        MOVE.w #$40, <RCRline
      bra main_loop



;Main end
;//...................................................................

;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
  .bank $02, "Subcode 1"
  .org $8000


  IncludeBinary Font.cell, "../base_func/video/print/font.dat"

Font.pal: .db $00,$00,$33,$01,$ff,$01,$ff,$01,$ff,$01,$ff,$01,$ff,$01,$f6,$01
Font.pal.size = sizeof(Font.pal)

    ;// Support files for MAIN
    .include "../base_func/init/InitHW.asm"

    .include "../lib/ColorTable/lib.asm"

;..............................................................................................................
;..............................................................................................................
;..............................................................................................................
;..............................................................................................................
;Bank 1 end


;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// Game enemies, player, and objects

  .bank $03, "Subcode"
  .org $6000



;..............................................................................................................
;..............................................................................................................
;..............................................................................................................
;..............................................................................................................

  ;Pad the Rom
  .bank $1f


;END OF FILE