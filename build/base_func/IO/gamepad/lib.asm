;###############################################
; 
; GAMEPAD support
;
; required variables:
;  
;    b1_sts
;    b2_sts
;    sl_sts
;    st_sts
;    up_sts
;    dn_sts
;    lf_sts
;    rh_sts
;
; value returned is : TRUE/FALSE 
;
;

  

READ_IO:
;
; new and improved
;
      pha
      phx
      lda #$01
      sta $1000
      lda #$03
      sta $1000
      lda #$01
      sta $1000
      pha
      pla
      nop
      nop
      lda $1000
      eor #$0f
      tax
      and #$01
      sta up_sts
      txa
      and #$04
      sta dn_sts
      txa
      and #$08
      sta lf_sts
      txa
      and #$02
      sta rh_sts
      
      
      stz $1000
      pha
      pla
      nop
      nop
      lda $1000
      eor #$0f
      tax
      and #$01
      sta b1_sts
      txa
      and #$02
      sta b2_sts
      txa
      and #$04
      sta sl_sts
      txa
      and #$08
      sta st_sts
 
      
.exit
      plx
      pla    
    rts 
;#end